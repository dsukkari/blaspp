BLAS++ Installation Notes
================================================================================

[TOC]

Synopsis
--------------------------------------------------------------------------------

Configure and compile the BLAS++ library and its tester,
then install the headers and library.

Option 1: Makefile

    make && make install

Option 2: CMake

    # In order to build tests, build TestSweeper, from
    # https://bitbucket.org/icl/testsweeper
    # In testsweeper directory:
    mkdir build && cd build
    cmake .. && make

    # In blaspp directory:
    mkdir build && cd build
    cmake -Dtestsweeper_DIR=/path/to/testsweeper/build ..
    make && make install


Environment variables (Makefile and CMake)
--------------------------------------------------------------------------------

Standard environment variables affect both Makefile (configure.py) and CMake.
These include:

    CXX                 C++ compiler
    CXXFLAGS            C++ compiler flags
    LDFLAGS             linker flags
    CPATH               compiler include search path
    LIBRARY_PATH        compile-time library search path
    LD_LIBRARY_PATH     runtime library search path
    DYLD_LIBRARY_PATH   runtime library search path on macOS


Options (Makefile and CMake)
--------------------------------------------------------------------------------

BLAS++ specific options include (all values are case insensitive):

    blas
        BLAS libraries to search for. One or more of:
        auto            search for all libraries (default)
        LibSci          Cray LibSci
        MKL             Intel MKL
        ESSL            IBM ESSL
        OpenBLAS        OpenBLAS
        Accelerate      Apple Accelerate framework
        ACML            AMD ACML (deprecated)
        generic         generic -lblas

    blas_int
        BLAS integer size to search for. One or more of:
        auto            search for both sizes (default)
        int             32-bit int (LP64 model)
        int64           64-bit int (ILP64 model)

    blas_threaded
        Whether to search for multi-threaded or sequential BLAS.
        Currently applies to Intel MKL and IBM ESSL. One of:
        auto            search for both threaded and sequential BLAS (default)
        yes             multi-threaded BLAS
        no              sequential BLAS

    blas_fortran
        Fortran interface to use. Currently applies only to Intel MKL.
        One or more of:
        auto            search for both interfaces (default)
        ifort           use Intel ifort interfaces (e.g., libmkl_intel_lp64)
        gfortran        use GNU gfortran interfaces (e.g., libmkl_gf_lp64)

    fortran_mangling
        (Makefile only; CMake always searches all manglings)
        BLAS and LAPACK are written in Fortran, which has a
        compiler-specific name mangling scheme: routine DGEMM is called
        dgemm_, dgemm, or DGEMM in the library. One or more of:
        auto            search all manglings (default)
        add_            add _ to names  (dgemm_)
        lower           lowercase names (dgemm)
        upper           uppercase names (DGEMM)

    BLAS_LIBRARIES
        Specify the exact BLAS libraries, overriding the built-in search. E.g.,
        cmake -DBLAS_LIBRARIES='-lopenblas' ..

    color
        Whether to use ANSI colors in output. One of:
        auto            uses color if output is a TTY
                        (default with Makefile; not support with CMake)
        yes             (default with CMake)
        no

With Makefile, options are specified as environment variables or on the
command line using `option=value` syntax, such as:

    python configure.py blas=mkl

With CMake, options are specified on the command line using
`-Doption=value` syntax (not as environment variables), such as:

    cmake -Dblas=mkl ..


Makefile Installation
--------------------------------------------------------------------------------

Available targets:

    make           - configures (if make.inc is missing),
                     then compiles the library and tester
    make config    - configures BLAS++, creating a make.inc file
    make lib       - compiles the library (lib/libblaspp.so)
    make tester    - compiles test/tester
    make docs      - generates documentation in docs/html/index.html
    make install   - installs the library and headers to ${prefix}
    make uninstall - remove installed library and headers from ${prefix}
    make clean     - deletes object (*.o) and library (*.a, *.so) files
    make distclean - also deletes make.inc and dependency files (*.d)


### Options

    make config [options]
    or
    python configure.py [options]

Runs the `configure.py` script to detect your compiler and library properties,
then creates a make.inc configuration file. You can also manually edit the
make.inc file. Options are name=value pairs to set variables.

Besides the Environment variables and Options listed above, additional
options include:

    static
        Whether to build as a static or shared library.
        0               shared library (default)
        1               static library

    prefix
        Where to install, default /opt/slate.
        Headers go   in ${prefix}/include,
        library goes in ${prefix}/lib${LIB_SUFFIX}

These can be set in your environment or on the command line, e.g.,

    python configure.py CXX=g++ prefix=/usr/local

Configure assumes environment variables are set so your compiler can find BLAS
libraries. For example:

    export LD_LIBRARY_PATH="/opt/my-blas/lib64"  # or DYLD_LIBRARY_PATH on macOS
    export LIBRARY_PATH="/opt/my-blas/lib64"
    export CPATH="/opt/my-blas/include"
    or
    export LDFLAGS="-L/opt/my-blas/lib64 -Wl,-rpath,/opt/my-blas/lib64"
    export CXXFLAGS="-I/opt/my-blas/include"

On some systems, loading the appropriate module will set these flags:

    module load my-blas


### Vendor notes

Intel MKL provides scripts to set these flags, e.g.:

    source /opt/intel/bin/compilervars.sh intel64
    or
    source /opt/intel/mkl/bin/mklvars.sh intel64


### Manual configuration

If you have a specific configuration that you want, set CXX, CXXFLAGS, LDFLAGS,
and LIBS, e.g.:

    export CXX="g++"
    export CXXFLAGS="-I${MKLROOT}/include -fopenmp"
    export LDFLAGS="-L${MKLROOT}/lib/intel64 -Wl,-rpath,${MKLROOT}/lib/intel64 -fopenmp"
    export LIBS="-lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lm"

These can also be set when running configure:

    make config CXX=g++ \
                CXXFLAGS="-I${MKLROOT}/include -fopenmp" \
                LDFLAGS="-L${MKLROOT}/lib/intel64 -Wl,-rpath,${MKLROOT}/lib/intel64 -fopenmp" \
                LIBS="-lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lm"

Note that all test programs are compiled with those options, so errors may cause
configure to fail.

If you experience unexpected problems, please see config/log.txt to diagnose the
issue. The log shows the option being tested, the exact command run, the
command's standard output (stdout), error output (stderr), and exit status. All
test files are in the config directory.


CMake Installation
--------------------------------------------------------------------------------

BLAS++ uses the TestSweeper library (https://bitbucket.org/icl/testsweeper)
to run its tests. This can be omitted, but then the tester will not be built.
First build TestSweeper:

    cd /path/to/testsweeper
    mkdir build && cd build
    cmake [options] ..
    make
    make install   # optional

Now build BLAS++ itself. The CMake script enforces an out-of-source
build. Create a build directory under the BLAS++ root directory:

    cd /path/to/blaspp
    mkdir build && cd build

Either TestSweeper can be installed, or the path to TestSweeper's build
directory should be given to the BLAS++ CMake.

    # If TestSweeper was installed, CMake should find it:
    cmake [options] ..

    # Else if TestSweeper was NOT installed, add its directory:
    cmake -Dtestsweeper_DIR=/path/to/testsweeper/build [options] ..

    make
    make install


### Options

Besides the Environment variables and Options listed above, additional
options include:

    use_openmp
        Whether to use OpenMP, if available.
        yes (default)
        no

    build_tests
        Whether to build test suite (test/tester).
        Requires TestSweeper, CBLAS, and LAPACK.
        yes (default)
        no

    use_cmake_find_blas
        Whether to use CMake's FindBLAS, instead of BLAS++ search.
        If BLA_VENDOR is set, it will use CMake's FindBLAS.
        yes
        no (default)

    BLA_VENDOR
        use CMake's FindBLAS, instead of BLAS++ search. For values, see:
        https://cmake.org/cmake/help/latest/module/FindBLAS.html

Standard CMake options include:

    BUILD_SHARED_LIBS
        Whether to build as a static or shared library.
        yes             shared library (default)
        no              static library

    CMAKE_INSTALL_PREFIX
        Where to install, default /opt/slate.
        Headers go   in ${prefix}/include,
        library goes in ${prefix}/lib

    CMAKE_BUILD_TYPE
        Type of build.
        Release
        Debug

With CMake, options are specified on the command line using
`-Doption=value` syntax (not as environment variables), such as:

    # in build directory
    cmake -Dblas=mkl -Dbuild_tests=no -DCMAKE_INSTALL_PREFIX=/usr/local ..

Alternatively, use the `ccmake` text-based interface or the CMake app GUI.

    # in build directory
    ccmake ..
    # Type 'c' to configure, then 'g' to generate Makefile

To re-configure CMake, you may need to delete CMake's cache:

    # in build directory
    rm CMakeCache.txt
    # or
    rm -rf *
    cmake [options] ..

To debug the build, set `VERBOSE`:

    # in build directory, after running cmake
    make VERBOSE=1
